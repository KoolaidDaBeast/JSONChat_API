<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class JSONUtili extends Controller
{
    public static function createJSONObject($keys, $values){
        $array = [];

        for ($i = 0; $i < count($keys); $i++){
            $array += array($keys[$i] => $values[$i]);
        }

        return json_encode($array);
    }
}
