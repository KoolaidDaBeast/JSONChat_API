<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Notify
 *
 * @property string $group_id
 * @property string $title
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Notify whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Notify whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Notify whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Notify whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Notify extends Model
{

    protected $table = "notify";

    //
    protected $fillable = [
        'group_id', 'title'
    ];
}
