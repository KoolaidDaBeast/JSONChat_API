<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Groups
 *
 * @property string $group_id
 * @property string $title
 * @property string $user_id
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Groups whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Groups whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Groups whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Groups whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Groups whereUserId($value)
 * @mixin \Eloquent
 */
class Groups extends Model
{
    protected $fillable = [
        'group_id', 'user_id', 'title'
    ];
}
