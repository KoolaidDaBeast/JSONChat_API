<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Messages
 *
 * @property string $group_id
 * @property string $user_id
 * @property string $read
 * @property string $message
 * @property \Carbon\Carbon|null $created_at
 * @property \Carbon\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Messages whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Messages whereGroupId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Messages whereMessage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Messages whereRead($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Messages whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Messages whereUserId($value)
 * @mixin \Eloquent
 */
class Messages extends Model
{
    protected $fillable = [
        'group_id', 'user_id', 'read', 'message'
    ];
}
