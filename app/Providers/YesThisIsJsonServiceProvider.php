<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

/**
 * Class YesThisIsJsonServiceProvider
 * @package App\Providers
 *
 *         ▄▀▀▀▀▀▀▀▄
 *        █▒▒▒▒▒▒▒▒▒█
 *       ▄▀▒▒▒▒▒▒▒▒▄▀
 *      █▒▒▒▒▒▒▒▒▒▒█
 *     ▄▀▒▄▄▄▒▄▄▄▒▒█
 *     █▒▒-▀-▒-▀-▒▒█
 *     █▒▒▒▒▒▒▒▒▒▒▒█
 *    ▄▀▒▒▒▀▄▄▄▀▒▒▒▒▀▀▄
 *  ▄▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▀▄
 * █▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒█▒▒█
 * ▀▄▀▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▒▀▄▀
 *   █▌▌▌▌▌▌▌▌▌▌▌▌▌▌▌▌█
 *   ▀█▌▌▌▌▌▌▌▌▌▌▌▌▌▌█▀
 *     █▒▒▌▌▌▌▌▌▌▌▌▒▒█
 *      ▀▀         ▀▀
 */
class YesThisIsJsonServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Str::startsWith($this->app->request->capture()->getRequestUri(), '/api/')) {
            $this->inject();
        }
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function inject() {
        app('request')->headers->add([
//            'Content-Type' => 'application/json',
            'Accept'       => 'application/json',
        ]);
    }
}
